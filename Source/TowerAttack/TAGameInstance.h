// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "TAGameInstance.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct FSpiderUpgrades
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Upgrades")
		float UpgradeSpeed = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Upgrades")
		float UpgradeHealth = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Upgrades")
		float UpgradePower = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Upgrades")
		float UpgradeDefense = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Upgrades")
		float UpgradeMagicDefense = 0;

	// For UI to grab updated information
	UPROPERTY(BlueprintReadWrite, Category = "Stats")
		float HP;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float Health = 50.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float Speed = 1.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float Power = 10.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float Defense = 10.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float MagicDefense = 10.f;
};

USTRUCT(BlueprintType)
struct FGruntlingUpgrades
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Upgrades")
		float UpgradeSpeed = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Upgrades")
		float UpgradeHealth = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Upgrades")
		float UpgradePower = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Upgrades")
		float UpgradeDefense = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Upgrades")
		float UpgradeMagicDefense = 0;

	// For UI to grab updated information
	UPROPERTY(BlueprintReadWrite, Category = "Stats")
		float HP;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float Health = 30.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float Speed = 1.2f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float Power = 1.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float Defense = 8.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float MagicDefense = 8.f;
};

USTRUCT(BlueprintType)
struct FMasterGruntUpgrades
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Upgrades")
		float UpgradeSpeed = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Upgrades")
		float UpgradeHealth = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Upgrades")
		float UpgradePower = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Upgrades")
		float UpgradeDefense = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Upgrades")
		float UpgradeMagicDefense = 0;

	// For UI to grab updated information
	UPROPERTY(BlueprintReadWrite, Category = "Stats")
		float HP;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float Health = 75.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float Speed = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float Power = 5.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float Defense = 15.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float MagicDefense = 15.f;
};

UCLASS()
class TOWERATTACK_API UTAGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Unit Upgrades")
		FSpiderUpgrades SpiderUpgrades;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Unit Upgrades")
		FGruntlingUpgrades GruntlingUpgrades;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Unit Upgrades")
		FMasterGruntUpgrades MasterGruntUpgrades;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Information")
		int32 Money;

	UFUNCTION(BlueprintCallable)
		void UpdateSpiderStats();
	UFUNCTION(BlueprintCallable)
		void UpdateGruntlingStats();
	UFUNCTION(BlueprintCallable)
		void UpdateMasterGruntStats();
	
};
