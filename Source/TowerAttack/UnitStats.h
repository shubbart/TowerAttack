// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TAGameInstance.h"
#include "Engine/World.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "UnitStats.generated.h"

UENUM(Blueprintable)
enum UnitType
{
	Spider,
	MasterGrunt,
	Gruntling
};


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOWERATTACK_API UUnitStats : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UUnitStats();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	float UpdateHP(float UpgradeValue);
	float UpdateSpeed(float UpgradeValue);
	float UpdateHealth(float UpgradeValue);
	float UpdatePower(float UpgradeValue);
	float UpdateDefense(float UpgradeValue);
	float UpdateMagicDefense(float UpgradeValue);

	UTAGameInstance* GameInstance;
	UCharacterMovementComponent* Movement;


public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Unit Information")
		TEnumAsByte<UnitType> MyUnitType;

	UPROPERTY(BlueprintReadWrite, Category = "Stats")
		float HP;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float Health;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float Speed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float Power;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float Defense;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float MagicDefense;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float AttackRange;

	UFUNCTION(BlueprintCallable)
		void UpdateStats();
		
};
