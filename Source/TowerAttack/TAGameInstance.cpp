// Fill out your copyright notice in the Description page of Project Settings.


#include "TAGameInstance.h"

void UTAGameInstance::UpdateSpiderStats()
{
	SpiderUpgrades.Health = SpiderUpgrades.Health + (SpiderUpgrades.UpgradeHealth * .1f);
	SpiderUpgrades.HP = 2 * (SpiderUpgrades.Health + (SpiderUpgrades.UpgradeHealth *.1f));
	SpiderUpgrades.Speed = SpiderUpgrades.Speed + (SpiderUpgrades.UpgradeSpeed * .1f);
	SpiderUpgrades.Power = SpiderUpgrades.Power + (SpiderUpgrades.UpgradePower * .1f);
	SpiderUpgrades.Defense = SpiderUpgrades.Defense + (SpiderUpgrades.UpgradeDefense * .1f);
	SpiderUpgrades.MagicDefense = SpiderUpgrades.MagicDefense + (SpiderUpgrades.UpgradeMagicDefense * .1f);
}

void UTAGameInstance::UpdateGruntlingStats()
{
	GruntlingUpgrades.Health = GruntlingUpgrades.Health + (GruntlingUpgrades.UpgradeHealth * .1f);
	GruntlingUpgrades.HP = 2 * (GruntlingUpgrades.Health + (GruntlingUpgrades.UpgradeHealth *.1f));
	GruntlingUpgrades.Speed = GruntlingUpgrades.Speed + (GruntlingUpgrades.UpgradeSpeed * .1f);
	GruntlingUpgrades.Power = GruntlingUpgrades.Power + (GruntlingUpgrades.UpgradePower * .1f);
	GruntlingUpgrades.Defense = GruntlingUpgrades.Defense + (GruntlingUpgrades.UpgradeDefense * .1f);
	GruntlingUpgrades.MagicDefense = GruntlingUpgrades.MagicDefense + (GruntlingUpgrades.UpgradeMagicDefense * .1f);
}

void UTAGameInstance::UpdateMasterGruntStats()
{
	MasterGruntUpgrades.Health = MasterGruntUpgrades.Health + (MasterGruntUpgrades.UpgradeHealth * .1f);
	MasterGruntUpgrades.HP = 2 * (MasterGruntUpgrades.Health + (MasterGruntUpgrades.UpgradeHealth *.1f));
	MasterGruntUpgrades.Speed = MasterGruntUpgrades.Speed + (MasterGruntUpgrades.UpgradeSpeed * .1f);
	MasterGruntUpgrades.Power = MasterGruntUpgrades.Power + (MasterGruntUpgrades.UpgradePower * .1f);
	MasterGruntUpgrades.Defense = MasterGruntUpgrades.Defense + (MasterGruntUpgrades.UpgradeDefense * .1f);
	MasterGruntUpgrades.MagicDefense = MasterGruntUpgrades.MagicDefense + (MasterGruntUpgrades.UpgradeMagicDefense * .1f);
}