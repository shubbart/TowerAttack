// Fill out your copyright notice in the Description page of Project Settings.


#include "UnitStats.h"

// Sets default values for this component's properties
UUnitStats::UUnitStats()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	

	// ...
}


// Called when the game starts
void UUnitStats::BeginPlay()
{
	Super::BeginPlay();
	GameInstance = Cast<UTAGameInstance>(GetWorld()->GetGameInstance());
	Movement = GetOwner()->FindComponentByClass<UCharacterMovementComponent>();
	UpdateStats();

	// ...
	
}


// Called every frame
void UUnitStats::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UUnitStats::UpdateStats()
{
	if(!GameInstance)
		GameInstance = Cast<UTAGameInstance>(GetWorld()->GetGameInstance());


	if (MyUnitType == UnitType::Spider)
	{
		// Remove this update when GameInstance updates happens through UI
		GameInstance->UpdateSpiderStats();
		Speed = GameInstance->SpiderUpgrades.Speed;
		Health = GameInstance->SpiderUpgrades.Health;
		Power = GameInstance->SpiderUpgrades.Power;
		Defense = GameInstance->SpiderUpgrades.Defense;
		MagicDefense = GameInstance->SpiderUpgrades.MagicDefense;
		HP = GameInstance->SpiderUpgrades.HP;
	}
	else if (MyUnitType == UnitType::Gruntling)
	{
		// Remove this update when GameInstance updates happens through UI
		GameInstance->UpdateGruntlingStats();
		Speed = GameInstance->GruntlingUpgrades.Speed;
		Health = GameInstance->GruntlingUpgrades.Health;
		Power = GameInstance->GruntlingUpgrades.Power;
		Defense = GameInstance->GruntlingUpgrades.Defense;
		MagicDefense = GameInstance->GruntlingUpgrades.MagicDefense;
		HP = GameInstance->GruntlingUpgrades.HP;
	}
	else if (MyUnitType == UnitType::MasterGrunt)
	{
		// Remove this update when GameInstance updates happens through UI
		GameInstance->UpdateMasterGruntStats();
		Speed = GameInstance->MasterGruntUpgrades.Speed;
		Health = GameInstance->MasterGruntUpgrades.Health;
		Power = GameInstance->MasterGruntUpgrades.Power;
		Defense = GameInstance->MasterGruntUpgrades.Defense;
		MagicDefense = GameInstance->MasterGruntUpgrades.MagicDefense;
		HP = GameInstance->MasterGruntUpgrades.HP;
	}

	Movement->MaxWalkSpeed = Movement->MaxWalkSpeed * Speed;
}

float UUnitStats::UpdateHP(float UpgradeValue)
{
	float CalculatedValue = UpgradeValue * .1f;

	return 2 * (Health + CalculatedValue);
}

float UUnitStats::UpdateSpeed(float UpgradeValue)
{
	float CalculatedValue = UpgradeValue * .1f;

	return Speed + CalculatedValue;
}

float UUnitStats::UpdateHealth(float UpgradeValue)
{
	float CalculatedValue = UpgradeValue * .1f;

	return Health + CalculatedValue;
}

float UUnitStats::UpdatePower(float UpgradeValue)
{
	float CalculatedValue = UpgradeValue * .1f;

	return Power + CalculatedValue;
}

float UUnitStats::UpdateDefense(float UpgradeValue)
{
	float CalculatedValue = UpgradeValue * .1f;

	return Defense + CalculatedValue;
}

float UUnitStats::UpdateMagicDefense(float UpgradeValue)
{
	float CalculatedValue = UpgradeValue * .1f;

	return MagicDefense + CalculatedValue;
}

